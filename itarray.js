let tabel = ["appel","peer"];
tabel.forEach(function(element){
  console.log(element);
})
// Het zelfde met een lambda
tabel.forEach(element => console.log(element));

let names = [ "Groucho", "Karl","Ingeborg"];
console.log(names.map(name =>	name + " Marx"	));
// ["Groucho Marx", "Karl Marx", "Ingeborg Marx"]
console.log([ "Groucho", "Karl","Ingeborg"]
  .filter(value => value.includes("a"))
);
// ["Karl"]

let nrs = [10, 20, 30, 40, 50];
console.log(nrs.reduce ( (acc,val) =>  acc+val));
// 150
